# OpenML dataset: Meta_Album_INS_Mini

https://www.openml.org/d/44306

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Insects Dataset (Mini)**
***
The original Insects dataset is created by the National Museum of Natural History, Paris (https://www.mnhn.fr/fr). It has more than 290 000 images in different sizes and orientations. The dataset has hierarchical classes which are listed from top to bottom as Order, Super-Family, Family, and Texa. Each image contains an insect in its natural environment or habitat, i.e, either on a flower or near to vegetation. The images are collected by the researchers and hundreds of volunteers from SPIPOLL Science project(https://www.spipoll.org/). The images are uploaded to a centralized server either by using the SPIPOLL website, Android application or IOS application. The preprocessed insect dataset is prepared from the original Insects dataset by carefully preprocessing the images, i.e., cropping the images from either side to make squared images. These cropped images are then resized into 128x128 using Open-CV with an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/INS.png)

**Meta Album ID**: SM_AM.INS  
**Meta Album URL**: [https://meta-album.github.io/datasets/INS.html](https://meta-album.github.io/datasets/INS.html)  
**Domain ID**: SM_AM  
**Domain Name**: Small Aninamls  
**Dataset ID**: INS  
**Dataset Name**: Insects  
**Short Description**: Insects dataset from Science Project SPIPOLL  
**\# Classes**: 104  
**\# Images**: 4110  
**Keywords**: insects, ecology  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC BY-NC 2.0  
**License URL(original data release)**: https://www.spipoll.org/mentions-legales
 
**License (Meta-Album data release)**: CC BY-NC 2.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/2.0/](https://creativecommons.org/licenses/by-nc/2.0/)  

**Source**: SPIPOLL; National Museum of Natural History, Paris  
**Source URL**: https://www.spipoll.org/  
  
**Original Author**: Gregoire Lois, Colin Fontaine, Jean-Francois Julien  
**Original contact**: contact@spipoll.org  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{insects, 
    title={Data quality and participant engagement in citizen science: comparing two approaches for monitoring pollinators in France and South Korea}, 
    author={Serret, Hortense and Deguines, Nicolas and Jang, Yikweon and Lois, Gregoire and Julliard, Romain}, 
    journal={Citizen Science: Theory and Practice}, 
    volume={4}, 
    number={1}, 
    pages={22}, 
    year={2019} 
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44276)  [[Extended]](https://www.openml.org/d/44340)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44306) of an [OpenML dataset](https://www.openml.org/d/44306). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44306/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44306/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44306/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

